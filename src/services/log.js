import { toast } from 'react-toastify'
// import * as Sentry from '@sentry/browser'

const annoyOptions = {
  hideProgressBar: true,
  draggable: false,
  autoClose: false,
  closeButton: false
}
// Sentry.init({ dsn: "https://*code*@sentry.io/1308323" })

/** Provides functions pertaining to logging */
const logService = {
  /**
   * Logs something
   * @param {string} message - what to log
   */
  log: console.log,

  /**
   * Alerts the user of something
   * @param {string} message - what to alert the user about
   * @param {function} onClose - what to do when the user acknowledges the message
   */
  alert(message, onClose) {
    toast(message, { onClose })
  },

  /**
   * Alerts the user of something and doesn't go away
   * @param {string} message - message to annoy user about
   * @param {function} onClose - what to do when the user acknowledges the message
   */
  annoy(message, onClose) {
    // noinspection JSCheckFunctionSignatures
    toast.warn(message, { ...annoyOptions, onClose })
  },

  /**
   * Alerts the user of an error
   * @param {string} message - the error message
   * @param {function()=} onClose - what to do when the user acknowledges the error
   */
  alertError(message, onClose) {
    toast.error(message, { onClose })
  }

  // /**
  //  * Sends an error to Sentry
  //  * @param {string|Error} error - the error to send to Sentry
  //  */
  // logSentry(error) {
  //   Sentry.captureException(error)
  // }
}

export default logService
