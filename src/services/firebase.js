import firebase from 'firebase/app'

import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const config = {
  apiKey: 'AIzaSyBmGkAryVWqZ4hSyq38zaF4Xqe8l46RZVI',
  authDomain: 'inyou-ministries.firebaseapp.com',
  databaseURL: "https://inyou-ministries.firebaseio.com",
  projectId: "inyou-ministries",
  storageBucket: "inyou-ministries.appspot.com",
  messagingSenderId: "945200926603"
};
firebase.initializeApp(config)

export default firebase
