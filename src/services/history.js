import { createBrowserHistory } from 'history'

/** A history object that uses the HTML5 history API */
const history = createBrowserHistory()

export default history
