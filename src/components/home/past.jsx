import { connect } from 'react-redux'
import Timeline from '../helpers/timeline'
import { fetchCampaigns } from '../../stores/campaigns/campaigns.actions'
import Module from '../../stores/module.enum'

const mapStateToProps = state => ({
  data: state[Module.CAMPAIGNS].data
})
const mapDispatchToProps = dispatch => ({
  fetchCampaigns: () => dispatch(fetchCampaigns())
})

class Past extends Timeline {
  title = '"InYou Window" Newsletter'
  cssModule = 'past'
  hideImage = true
  componentDidMount() {
    const { fetchCampaigns } = this.props
    fetchCampaigns()
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Past)
