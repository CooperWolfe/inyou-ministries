import React, { Component } from 'react'
import { connect } from 'react-redux'
import Past from './past'
import Present from './present'
import Future from './future'
import './news.css'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class News extends Component {
  render() {
    return (
      <div className='news row'>
        {/*Past*/}
        <hr className='d-lg-none'/>
        <Past className='past col-lg-4'/>

        {/*Present*/}
        <hr className='d-lg-none'/>
        <Present className='present col-lg-4'/>

        {/*Future*/}
        <hr className='d-lg-none'/>
        <Future className='future col-lg-4'/>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(News)
