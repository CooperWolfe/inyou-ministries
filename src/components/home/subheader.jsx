import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './subheader.css'

class Subheader extends Component {
  state = {}

  render() {
    return (
      <div className='subheader row'>
        <div className='text col-lg-8'>
          <h4>
            “For the kingdom of God does not consist in words…but in power.” ~ 1 Cor 4:20
          </h4>
        </div>
        <div className='button col-lg-4'>
          <Link to='/curricula'>
            Shop Our Curricula <i className='fa fa-book' />
          </Link>
        </div>
      </div>
    )
  }
}

export default Subheader
