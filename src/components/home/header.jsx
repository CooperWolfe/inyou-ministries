import React from 'react'
import SizingComponent from '../helpers/sizingComponent'
import { Link } from 'react-router-dom'
import Parallax from '../common/parallax'
import Cover from '../common/cover'
import './header.css'

class Header extends SizingComponent {
  state = { ...SizingComponent.defaultState }

  _BREAKPOINT = 768

  renderHeaderContent = () => {
    return (
      <div className='header'>
        <h1>
          <strong>I</strong>N<strong>Y</strong>OU <strong>M</strong>INISTRIES
        </h1>
        <h2>
          “Leading people to <strong>EXPERIENCE</strong> & <strong>EXPRESS</strong> Jesus in the <strong>NOW</strong>!”
        </h2>
        <Link to='/contribute' className='btn mt-1'>
          Contribute
        </Link>
      </div>
    )
  }
  render() {
    const { width } = this.state

    const imageUrl = 'https://inyou.org/wp-content/uploads/2017/05/open-tomb.png'
    const large = width > this._BREAKPOINT

    return large
      ? <Parallax imageUrl={imageUrl}>
        { this.renderHeaderContent() }
      </Parallax>
      : <Cover imageUrl={imageUrl}>
        { this.renderHeaderContent() }
      </Cover>
  }
}

export default Header
