import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { fetchThreeCs } from '../../stores/three-cs/threeCs.actions'
import CCard from './cCard'
import './threeCs.css'

const mapStateToProps = state => ({
  threeCs: state.threeCs.list
})
const mapDispatchToProps = dispatch => ({
  fetchThreeCs: () => dispatch(fetchThreeCs())
})

class ThreeCs extends Component {
  componentDidMount() {
    const { fetchThreeCs } = this.props
    fetchThreeCs()
  }

  render() {
    const { threeCs } = this.props
    return (
      <div className='three-cs row'>
        { threeCs.map((c, i) => <Fragment key={i}>
          <hr className='d-lg-none' />
          <CCard className='col-lg-4' c={c} />
        </Fragment> )}
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThreeCs)
