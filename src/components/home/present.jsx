import { connect } from 'react-redux'
import Timeline from '../helpers/timeline'
import Module from '../../stores/module.enum'
import { fetchPosts } from '../../stores/posts/posts.actions'

const mapStateToProps = state => ({
  data: state[Module.POSTS].data
})
const mapDispatchToProps = dispatch => ({
  fetchPosts: () => dispatch(fetchPosts())
})

class Present extends Timeline {
  title = 'InYou in the NOW!'
  cssModule = 'present'
  componentDidMount() {
    const { fetchPosts } = this.props
    fetchPosts()
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Present)
