import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Timeline from '../helpers/timeline'
import Module from '../../stores/module.enum'
import './future.css'
import { fetchEvents } from '../../stores/events/events.actions'
import markdownToHtml from 'markdown-to-html-converter'

const mapStateToProps = state => ({
  content: state[Module.EVENTS].data
})
const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
})

class Future extends Timeline {
  title = 'Upcoming Events'
  cssModule = 'future'

  componentDidMount() {
    const { fetchEvents } = this.props
    fetchEvents()
  }

  render() {
    const { content } = this.props
    return this.renderCard(
      <Fragment>
        { this.renderHeader() }
        <div className='events' dangerouslySetInnerHTML={{ __html: markdownToHtml(content) }}/>
      </Fragment>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Future)
