import React, { Component } from 'react'
import Subheader from './subheader'
import Header from './header'
import ThreeCs from './threeCs'
import News from './news'

class Home extends Component {
  render() {
    return <div className='home'>
      <Header/>
      <Subheader />
      <div className='bg-light container'>
        <ThreeCs/>
      </div>
      <div className='bg-light container'>
        <News/>
      </div>
    </div>
  }
}

export default Home
