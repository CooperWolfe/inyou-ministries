import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { capitalize } from '../../helpers/string.helper'
import { Link } from 'react-router-dom'
import './cCard.css'

class CCard extends Component {
  static propTypes = {
    c: PropTypes.object.isRequired
  }

  render() {
    const { c, ...rest } = this.props

    const title = capitalize(c.id)

    return (
      <div {...rest}>
        <div className='card ccard'>
          <Link to={`/${c.id}`} className='card-img-top'>
            <img src={c.imageUrl} alt={title} />
          </Link>
          <div className='card-body'>
            <h4 className='card-title'>
              <Link to={`/${c.id}`}>{ title }</Link>
            </h4>
            <p className='card-text'>{ c.description }</p>
          </div>
        </div>
      </div>
    )
  }
}

export default CCard
