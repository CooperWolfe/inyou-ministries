import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import NotFound from './notFound'
import Home from './home/home'
import Conference from './conference/conference'
import Counsel from './counsel/counsel'
import Curricula from './curricula/curricula'
import Contribute from './contribute/contribute'
import Login from './login'

class Content extends Component {

  getRouteMap = () => {
    return [
      { path: '/', component: Home, exact: true },
      { path: '/conference', component: Conference },
      { path: '/counsel', component: Counsel },
      { path: '/curricula', component: Curricula },
      { path: '/contribute', component: Contribute },
      { path: '/login', component: Login }
    ]
  }

  render() {
    return (
      <Switch>

        { this.getRouteMap().map((route, i) => (
          <Route key={i} { ...route } />
        )) }

        <Route path='/not-found' render={() => <NotFound className='c-accent' linkStyle={{ color: 'var(--light)' }}/>}/>
        <Redirect from='/' to='/not-found' />
      </Switch>
    )
  }
}

export default Content
