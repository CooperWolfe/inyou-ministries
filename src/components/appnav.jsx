import React from 'react'
import { connect } from 'react-redux'
import Navbar from './helpers/navbar'

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated
})

class Appnav extends Navbar {
  renderBrand = () => {
    return <img src='/logo.png' alt='InYou Ministries' height={75} />
  }
  renderRight = () => {
    const { authenticated } = this.props

    const links = [
      { text: 'Home', url: '/', exact: true },
      { text: 'Conference', url: '/conference' },
      { text: 'Counsel', url: '/counsel' },
      { text: 'Curricula', url: '/curricula' },
      { text: 'Contribute', url: '/contribute' },
      { text: <i className='fa fa-user-circle'/>, url: '/login' }
    ]
    if (authenticated)
      links.forEach(link => link.url = `/admin${link.url}`)

    return this.renderLinks(links)
  }
}

export default connect(mapStateToProps)(Appnav)
