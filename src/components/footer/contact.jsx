import React, { Component } from 'react'
import { connect } from 'react-redux'
import Module from '../../stores/module.enum'
import { toPhoneFormat } from '../../helpers/string.helper'
import './contact.css'

const mapStateToProps = state => ({
  contactInfo: state[Module.CONTACT_INFO].data
})

class Contact extends Component {
  renderContact = (link, faIcon, text) => {
    return (
      <a href={link}>
        <i className={`fa fa-${faIcon}`}/> { text }
      </a>
    )
  }
  render() {
    const contactInfo = this.props.contactInfo
    return (
      <div className='contact'>
        <h4>Address</h4>
        <a href={contactInfo.addressUrl}>
          InYou Ministries<br/>
          { contactInfo.address1 }<br/>
          { contactInfo.address2 }<br/>
        </a>

        <h4>Mailing Address</h4>
        <p>{ contactInfo.mailingAddress }</p>

        <h4>Contact</h4>
        <p>
          { contactInfo.email && this.renderContact(`mailto:${contactInfo.email}`, 'envelope', contactInfo.email) }<br/>
          { contactInfo.phone && this.renderContact(`tel:${contactInfo.phone}`, 'phone', toPhoneFormat(contactInfo.phone, '3.3.4')) }<br/>
        </p>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Contact)
