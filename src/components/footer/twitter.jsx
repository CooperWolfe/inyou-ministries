import React, { Component } from 'react'
import { TwitterTimelineEmbed } from 'react-twitter-embed'

class Twitter extends Component {
  render() {
    return (
      <div className='twitter'>
        <TwitterTimelineEmbed sourceType='profile' screenName='Jesus_inthe_NOW' options={{ height: 400 }}/>
      </div>
    )
  }
}

export default Twitter
