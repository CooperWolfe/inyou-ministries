import React, { Component } from 'react'
import { connect } from 'react-redux'
import './footer.css'
import Contact from './contact'
import Twitter from './twitter'
import Facebook from './facebook'
import { fetchContactInfo } from '../../stores/contact-info/contact-info.actions'
import Newsletter from './newsletter'

const mapDispatchToProps = dispatch => ({
  fetchContactInfo: () => dispatch(fetchContactInfo())
})

class Footer extends Component {
  componentDidMount() {
    const { fetchContactInfo } = this.props
    fetchContactInfo()
  }

  render() {
    return (
      <div className='footer container'>
        <div className='row'>
          <div className='col-md-6 col-lg-3'>
            <h4>GET IN TOUCH</h4>
            <Contact/>
          </div>
          <div className='col-md-6 col-lg-3'>
            <h4>NEWSLETTER</h4>
            <Newsletter/>
          </div>
          <div className='col-md-6 col-lg-3'>
            <h4>FACEBOOK</h4>
            <Facebook/>
          </div>
          <div className='col-md-6 col-lg-3'>
            <h4>TWITTER</h4>
            <Twitter/>
          </div>
        </div>
        <p>
          © 2019 InYou Ministries. All rights reserved. | <a href='https://wolfewebdev.com'>Created by Wolfe Web Dev</a>
        </p>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(Footer)
