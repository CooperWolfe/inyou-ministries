import React from 'react'
import MailchimpSubscribe from 'react-mailchimp-subscribe'
import Form from '../helpers/form'
import Joi from 'joi-browser'
import './newsletter.css'
import Loader from '../common/loader'

class Newsletter extends Form {
  state = {
    ...Form.defaultState,
    data: {
      FNAME: '',
      LNAME: '',
      EMAIL: ''
    }
  }
  schema = {
    FNAME: Joi.string().required(),
    LNAME: Joi.string().required(),
    EMAIL: Joi.string().email().required()
  }

  renderForm = ({ subscribe, status, message }) => {
    return (
      <form className='newsletter' onSubmit={e => this.handleSubmit(e, subscribe)}>
        { this.renderField('First Name', 'FNAME') }
        { this.renderField('Last Name', 'LNAME') }
        { this.renderField('Email', 'EMAIL', 'email') }
        { status === 'error' && <small className='error' dangerouslySetInnerHTML={{ __html: message }} /> }
        { status === 'success' && <small className='success' dangerouslySetInnerHTML={{ __html: message }} /> }
        { this.renderButton('submit', 'outline-success', status === 'sending' ? <Loader /> : 'Subscribe') }
      </form>
    )
  }
  render() {
    return (
      <MailchimpSubscribe url='https://inyou.us6.list-manage.com/subscribe/post?u=cf0bbade3e17bb6512806aba3&amp;id=ee169b7b3e'
                          render={this.renderForm}/>
    )
  }

  doSubmit = (_, subscribe) => {
    const { data } = this.state
    subscribe(data)
  }
}

export default Newsletter
