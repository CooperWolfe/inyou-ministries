import React, { Component } from 'react'
import { FacebookProvider, Page } from 'react-facebook'
import './facebook.css'

class Facebook extends Component {
  render() {
    return (
      <div className='facebook'>
        <FacebookProvider appId='2342118589141023'>
          <Page href='https://www.facebook.com/InYou-Ministries-270792209619016/' tabs='timeline'/>
        </FacebookProvider>
      </div>
    )
  }
}

export default Facebook
