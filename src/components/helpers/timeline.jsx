import React, { Component, Fragment } from 'react'
import dateFormat from 'dateformat'
import './timeline.css'

class Timeline extends Component {
  renderHeader = () => {
    return <h5 className='text-center card-header c-primary'>{ this.title }</h5>
  }
  /** @param {Array<News>} news */
  renderNews = news => {
    return (
      <div className='card-body'>
        <ul className='list-group list-group-flush'>
          { news.map((article, i) => (
            <li className='list-group-item c-primary p-0' key={i}>
              <a style={{ padding: '.75rem 0' }} className='d-flex' href={article.url} title={article.subject}>
                { !this.hideImage && <img src={article.imageUrl} alt={article.subject} width={60} height={60} style={{ marginRight: '1rem', minWidth: 60 }} /> }
                <div style={{ minWidth: 0 }}>
                  <h6 className='o-50'>{ dateFormat(article.date, 'mmmm dd, yyyy') }</h6>
                  <h4>{ article.subject }</h4>
                </div>
              </a>
            </li>
          )) }
        </ul>
      </div>
    )
  }

  renderCard = children => {
    const { className } = this.props
    return (
      <div className={`${this.cssModule || ''} timeline ${className}`}>
        <div className='card' style={{ border: 'none' }}>
          { children }
        </div>
      </div>
    )
  }
  render() {
    const { data } = this.props
    return this.renderCard(
      <Fragment>
        { this.renderHeader() }
        { this.renderNews(data) }
      </Fragment>
    )
  }
}

export default Timeline
