import { Component } from 'react'

const defaultState = {
  width: 0
}

class SizingComponent extends Component {
  state = { ...defaultState }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }
  componentWillMount() {
    this.updateDimensions()
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }
}
SizingComponent.defaultState = defaultState

export default SizingComponent
