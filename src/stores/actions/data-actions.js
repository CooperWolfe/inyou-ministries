import firebase from '../../services/firebase'
import AsyncActions from './async-actions'

/** Base for all actions that fetch a single data point */
export default class DataActions extends AsyncActions {
  /**
   * A reference to the module's firebase document
   * @private
   */
  _docRef
  /**
   * The ID of the collection in firebase
   * @protected string
   */
  $collectionId
  /**
   * The ID of the document in firebase
   * @protected string
   */
  $docId

  /**
   * Gets the document reference for this module
   * @protected
   * @returns DocumentReference
   */
  $getDocRef() {
    if (!this._docRef)
      this._docRef = firebase.firestore().collection(this.$collectionId).doc(this.$docId)
    return this._docRef
  }
  /** @inheritDoc */
  $convertDocToData(documentSnapshot) {
    return {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
  }

  // Strategies
  /** @inheritDoc */
  $optimistic(promise, dispatch, getState, onError = undefined) {
    dispatch(this.request())
    const previousData = getState()[this.$module].data
    return promise
      .then(() => dispatch(this.receive()))
      .catch(err => {
        dispatch(this.set(previousData))
        return dispatch(this.error(err))
      })
  }

  // Synchronous
  /**
   * Sets the data
   * @param {object} data - the data to set
   */
  set = data => ({
    type: DataActions.SET,
    module: this.$module,
    data
  })

  // Asynchronous
  /** Fetches the data */
  fetch = () => {
    return dispatch => this.$pessimistic(
      this.$getDocRef().get()
        .then(this.$convertDocToData)
        .then(data => dispatch(this.set(data))),
      dispatch
    )
  }

  // Static
  static SET = 'SET'
}
