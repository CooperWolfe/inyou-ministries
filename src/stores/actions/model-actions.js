import DataActions from './data-actions'
import NotImplementedError from '../../errors/notImplemented.error'

/** Base for all actions that fetch a single model data point */
export default class ModelActions extends DataActions {
  /**
   * @inheritDoc
   * @returns {*}
   */
  $convertDocToData(documentSnapshot) {
    throw new NotImplementedError('$convertDocToData')
  }

  // Synchronous
  /**
   * Updates data
   * @param {Model} data - the data to update
   */
  update = data => ({
    type: ModelActions.UPDATE,
    module: this.$module,
    data
  })

  // Async
  /**
   * Updates data persistently
   * @param {Model} data - the data to update on the backend
   */
  post = data => {
    return (dispatch, getState) => {
      dispatch(this.update(data))
      return this.$optimistic(
        this.$getDocRef().update(data.toPersistenceObject()),
        dispatch,
        getState
      )
    }
  }

  // Constants
  static UPDATE = 'UPDATE'
}
