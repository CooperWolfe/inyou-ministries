import Actions from './actions'
import NotImplementedError from '../../errors/notImplemented.error'

/** Base for all asynchronous actions with wrapper methods for asynchronous action execution strategies */
export default class AsyncActions extends Actions {
  /**
   * Strategy that ensures the backend is updated before updating the frontend to match it
   * @protected
   * @param {Promise} promise - the asynchronous action to execute
   * @param {function(*): Action} dispatch - the dispatcher
   * @param {function(error: Error)=} onError - error callback
   * @returns {Promise<Action>} a promise that the pessimistic update will complete as expected
   */
  $pessimistic(promise, dispatch, onError) {
    dispatch(this.request())
    return promise
      .then(() => dispatch(this.receive()))
      .catch(error => {
        onError && onError(error)
        dispatch(this.error(error))
      })
  }

  /**
   * Strategy that updates the frontend before the backend and corrects the frontend if any errors occur
   * @protected
   * @param {Promise} promise - the asynchronous action to execute
   * @param {function(*): Action} dispatch - the dispatcher
   * @param {function(): object} getState - the state accessor
   * @param {function(error: Error)=} onError - error callback
   * @returns {Promise<Action>} a promise that the optimistic update will complete as expected
   */
  $optimistic(promise, dispatch, getState, onError) { throw new NotImplementedError('$optimistic') }

  // noinspection JSMethodCanBeStatic
  /**
   * Converts a document snapshot to desired data
   * @protected
   * @param {*} documentSnapshot - The document snapshot to convert
   * @returns {*} the data you want from the document reference
   */
  $convertDocToData(documentSnapshot) { throw new NotImplementedError('$convertDocToData') }
}
