import AsyncActions from './async-actions'
import firebase from '../../services/firebase'

/** Base for all actions pertaining to lists of models */
export default class ModelListActions extends AsyncActions {
  /** @private */
  _collectionRef
  /**
   * The ID of the firebase collection
   * @protected string
   */
  $collectionId

  /**
   * Gets the module's collection reference
   * @protected
   * @returns {CollectionReference} this module's collection reference
   */
  $getCollectionRef() {
    if (!this._collectionRef)
      this._collectionRef = firebase.firestore().collection(this.$collectionId)
    return this._collectionRef
  }

  // Strategies
  /** @inheritDoc */
  $optimistic(promise, dispatch, getState, onError = undefined) {
    dispatch(this.request())
    const previousList = getState()[this.$module].list
    return promise
      .then(() => dispatch(this.receive()))
      .catch(err => {
        dispatch(this.setList(previousList))
        return dispatch(this.error(err))
      })
  }

  // Synchronous
  /**
   * Sets the list
   * @param {Array<Model>} list - what to set the list to
   */
  setList = list => ({
    type: ModelListActions.SET_LIST,
    module: this.$module,
    list
  })
  /**
   * Appends an item to the list
   * @param {Model} item - the action to append
   */
  append = item => ({
    type: ModelListActions.APPEND,
    module: this.$module,
    item
  })
  /**
   * Prepends an item to the list
   * @param {Model} item - the item to prepend
   */
  prepend = item => ({
    type: ModelListActions.PREPEND,
    module: this.$module,
    item
  })
  /**
   * Updates an item in the list, using the item's ID to know which one to update
   * @param {Model} item - the item to update
   */
  update = item => ({
    type: ModelListActions.UPDATE,
    module: this.$module,
    item
  })
  /**
   * Removes an item from the list
   * @param {string} id - the ID of the item to remove
   */
  remove = id => ({
    type: ModelListActions.REMOVE,
    module: this.$module,
    id
  })
  /**
   * Selects one of the items in the list by ID
   * @param {string} id - the ID of the item to select
   */
  select = id => ({
    type: ModelListActions.SELECT,
    module: this.$module,
    id
  })
  /** Deselects all items in the list */
  deselect = () => ({
    type: ModelListActions.DESELECT,
    module: this.$module
  })

  // Async
  /** Fetches the list */
  fetch = () => {
    console.log()
    return dispatch => this.$pessimistic(
      this.$getCollectionRef().get()
        .then(res => res.docs.map(this.$convertDocToData))
        .then(list => dispatch(this.setList(list))),
      dispatch
    )
  }
  /**
   * Adds an item to the list persistently
   * @param {Model} item - the item to store on the backend
   */
  store = item => {
    return (dispatch, getState) => {
      dispatch(this.append(item))
      return this.$optimistic(
        this.$getCollectionRef().doc(item.id).set(item.toPersistenceObject()),
        dispatch,
        getState
      )
    }
  }
  /**
   * Updates item persistently, using the item's ID to know which item to update
   * @param {Model} item - the item to update on the backend
   */
  post = item => {
    return (dispatch, getState) => this.$optimistic(
      this.$getCollectionRef().doc(item.id).update(item.toPersistenceObject()),
      dispatch,
      getState
    )
  }
  /**
   * Removes an item from the list persistently
   * @param {string} id - the ID of the item to delete on the backend
   */
  delete = id => {
    return (dispatch, getState) => this.$optimistic(
      this.$getCollectionRef().doc(id).delete(),
      dispatch,
      getState
    )
  }

  // Constants
  static SET_LIST = 'SET_LIST'
  static APPEND = 'APPEND'
  static PREPEND = 'PREPEND'
  static UPDATE = 'UPDATE'
  static REMOVE = 'REMOVE'
  static SELECT = 'SELECT'
  static DESELECT = 'DESELECT'
}
