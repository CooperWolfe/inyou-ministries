import Reducer from '../reducers/reducer'
import Module from '../module.enum'
import firebase from '../../services/firebase'
import AuthActions from './auth.actions'

/** Reducer for authentication */
class AuthReducer extends Reducer {
  /** @inheritDoc */
  $module = Module.AUTH

  /** Authenticate reducer */
  authenticate = () => ({ authenticated: true })
  /** Unauthenticate reducer */
  unauthenticate = () => ({ authenticated: false })

  /** @inheritDoc */
  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [AuthActions.AUTHENTICATE]: this.authenticate,
      [AuthActions.UNAUTHENTICATE]: this.unauthenticate
    }
  }
  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      authenticated: !!firebase.auth().currentUser
    }
  }
}

/**
 * A reducer for authentication
 * @type {AuthReducer}
 */
const authReducer = new AuthReducer()

export default authReducer
