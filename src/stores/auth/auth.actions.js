import Module from '../module.enum'
import AsyncActions from '../actions/async-actions'
import firebase from '../../services/firebase'
import logService from '../../services/log'

/** Actions pertaining to authentication */
export default class AuthActions extends AsyncActions {
  /** @inheritDoc */
  $module = Module.AUTH

  /** Authenticates session */
  authenticate = () => ({
    type: 'AUTHENTICATE',
    module: this.$module
  })
  /** Unauthenticates session */
  unauthenticate = () => ({
    type: 'UNAUTHENTICATE',
    module: this.$module
  })

  /**
   * Logs user in
   * @param {string} email - the user's email
   * @param {string} password - the user's password
   */
  login = (email, password) => {
    return dispatch => this.$pessimistic(firebase.auth().signInWithEmailAndPassword(email, password), dispatch, err => {
      if (err.code === 'auth/user-not-found')
        logService.alertError('Login failed')
    })
  }
  /** Logs user out */
  logout = () => dispatch => this.$pessimistic(firebase.auth().signOut(), dispatch)

  static AUTHENTICATE = 'AUTHENTICATE'
  static UNAUTHENTICATE = 'UNAUTHENTICATE'
}

/**
 * Actions pertaining to authentication
 * @type {AuthActions}
 */
const authActions = new AuthActions()

export const login = authActions.login
export const logout = authActions.logout
