import ModelReducer from '../reducers/model-reducer'
import Module from '../module.enum'

/** Reducer for the contact info module */
class ContactInfoReducer extends ModelReducer {
  /** @inheritDoc */
  $module = Module.CONTACT_INFO

  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      data: {}
    }
  }
}

/**
 * A reducer for the contact info module
 * @type {ContactInfoReducer}
 */
const contactInfoReducer = new ContactInfoReducer()

export default contactInfoReducer
