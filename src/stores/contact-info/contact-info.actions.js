import ModelActions from '../actions/model-actions'
import Module from '../module.enum'
import ContactInfo from '../../model/contact-info.model'

/** Actions pertaining to contact information */
class ContactInfoActions extends ModelActions {
  /** @inheritDoc */
  $collectionId = 'site-data'
  /** @inheritDoc */
  $docId = 'contact-info'
  /** @inheritDoc */
  $module = Module.CONTACT_INFO

  /** @inheritDoc */
  $convertDocToData(documentSnapshot) {
    return ContactInfo.fromDocumentSnapshot(documentSnapshot)
  }
}

/**
 * Actions pertaining to contact information
 * @type {ContactInfoActions}
 */
const contactInfoActions = new ContactInfoActions()

export const fetchContactInfo = contactInfoActions.fetch
export const updateContactInfo = contactInfoActions.post
