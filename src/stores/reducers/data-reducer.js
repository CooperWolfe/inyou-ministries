import Reducer from './reducer'
import Actions from '../actions/data-actions'

/** Base for all reducers with only one data point */
export default class DataReducer extends Reducer {
  /** Set reducer */
  set = (state, { data }) => ({ ...state, data })

  /** @inheritDoc */
  $getHandlerMap() {
    return {
      ...super.$getHandlerMap(),
      [Actions.SET]: this.set
    }
  }

  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      data: null
    }
  }
}
