import AsyncActions from '../actions/async-actions'
import Module from '../module.enum'
import axios from 'axios'
import Post from '../../model/post.model'

/** All actions pertaining to Instagram posts */
class PostsActions extends AsyncActions {
  /** @inheritDoc */
  $module = Module.POSTS

  /**
   * Sets the posts
   * @param {Array<Post>} posts - the posts to set the data to
   */
  set = posts => ({
    module: this.$module,
    type: PostsActions.SET,
    data: posts
  })

  /**
   * Fetches the posts from instagram
   */
  fetchPosts = () => {
    return dispatch => {
      return this.$pessimistic(
        axios.get('https://us-central1-inyou-ministries.cloudfunctions.net/fetchPosts')
          .then(({ data: posts }) => dispatch(this.set(posts.map(Post.fromObject)))),
        dispatch
      )
    }
  }

  static SET = 'SET'
}

/**
 * Actions pertaining to Instagram posts
 * @type {PostsActions}
 */
const postsActions = new PostsActions()

export const fetchPosts = postsActions.fetchPosts
