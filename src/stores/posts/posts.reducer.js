import Module from '../module.enum'
import DataReducer from '../reducers/data-reducer'

/** Reducer pertaining to Instagram posts */
class PostsReducer extends DataReducer {
  /** @inheritDoc */
  $module = Module.POSTS

  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      data: []
    }
  }
}

/**
 * A reducer for Instagram posts
 * @type {PostsReducer}
 */
const postsReducer = new PostsReducer()

export default postsReducer
