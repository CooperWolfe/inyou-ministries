import ModelListActions from '../actions/model-list-actions'
import Module from '../module.enum'
import C from '../../model/c.model'

class ThreeCsActions extends ModelListActions {
  /** @inheritDoc */
  $module = Module.THREE_CS
  /** @inheritDoc */
  $collectionId = 'three-cs'

  /** @inheritDoc */
  $convertDocToData(documentSnapshot) {
    return C.fromDocumentSnapshot(documentSnapshot)
  }
}

const threeCsActions = new ThreeCsActions()

export const fetchThreeCs = threeCsActions.fetch
export const updateC = threeCsActions.post
