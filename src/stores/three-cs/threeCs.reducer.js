import ModelListReducer from '../reducers/model-list-reducer'
import Module from '../module.enum'

/** Reducer for the three C's: conference, counsel, and curricula */
class ThreeCsReducer extends ModelListReducer {
  /** @inheritDoc */
  $module = Module.THREE_CS
}

/**
 * A Reducer for the three C's: conference, counsel, and curricula
 * @type {ThreeCsReducer}
 */
const threeCsReducer = new ThreeCsReducer()

export default threeCsReducer
