import Module from './module.enum'
import authReducer from './auth/auth.reducer'
import campaignsReducer from './campaigns/campaigns.reducer'
import contactInfoReducer from './contact-info/contact-info.reducer'
import eventsReducer from './events/events.reducer'
import postsReducer from './posts/posts.reducer'
import threeCsReducer from './three-cs/threeCs.reducer'

const initialState = {
  [Module.AUTH]: authReducer.getInitialState(),
  [Module.CAMPAIGNS]: campaignsReducer.getInitialState(),
  [Module.CONTACT_INFO]: contactInfoReducer.getInitialState(),
  [Module.EVENTS]: eventsReducer.getInitialState(),
  [Module.POSTS]: postsReducer.getInitialState(),
  [Module.THREE_CS]: threeCsReducer.getInitialState()
}

export default function reducer(state = initialState, action) {
  switch (action.module) {
  case Module.AUTH: return authReducer.reduce(state, action)
  case Module.CAMPAIGNS: return campaignsReducer.reduce(state, action)
  case Module.CONTACT_INFO: return contactInfoReducer.reduce(state, action)
  case Module.EVENTS: return eventsReducer.reduce(state, action)
  case Module.POSTS: return postsReducer.reduce(state, action)
  case Module.THREE_CS: return threeCsReducer.reduce(state, action)
  default: return state
  }
}
