/**
 * Module names
 * @enum {string}
 */
const Module = {
  AUTH: 'auth',
  CAMPAIGNS: 'campaigns',
  CONTACT_INFO: 'contactInfo',
  EVENTS: 'events',
  POSTS: 'posts',
  THREE_CS: 'threeCs'
}

export default Module
