import AsyncActions from '../actions/async-actions'
import Module from '../module.enum'
import axios from 'axios'
import Campaign from '../../model/campaign.model'

/** All actions pertaining to MailChimp campaigns */
class CampaignsActions extends AsyncActions {
  /** @inheritDoc */
  $module = Module.CAMPAIGNS

  /**
   * Sets the campaigns
   * @param {Array<Campaign>} campaigns - the campaigns to set
   */
  set = campaigns => ({
    module: this.$module,
    type: CampaignsActions.SET,
    data: campaigns
  })

  /**
   * Fetches the campaigns from Mailchimp
   */
  fetchCampaigns = () => {
    return dispatch => {
      return this.$pessimistic(
        axios.get('https://us-central1-inyou-ministries.cloudfunctions.net/fetchCampaigns')
          .then(({ data: campaigns }) => dispatch(this.set(campaigns.map(Campaign.fromObject)))),
        dispatch
      )
    }
  }

  static SET = 'SET'
}

/**
 * Actions pertaining to MailChimp campaigns
 * @type {CampaignsActions}
 */
const campaignsActions = new CampaignsActions()

export const fetchCampaigns = campaignsActions.fetchCampaigns
