import Module from '../module.enum'
import DataReducer from '../reducers/data-reducer'

/** Reducer pertaining to MailChimp campaigns */
class CampaignsReducer extends DataReducer {
  /** @inheritDoc */
  $module = Module.CAMPAIGNS

  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      data: []
    }
  }
}

/**
 * A reducer for MailChimp campaigns
 * @type {CampaignsReducer}
 */
const campaignsReducer = new CampaignsReducer()

export default campaignsReducer
