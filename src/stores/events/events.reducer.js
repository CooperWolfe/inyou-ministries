import DataReducer from '../reducers/data-reducer'
import Module from '../module.enum'

/** A reducer pertaining to upcoming events */
class EventsReducer extends DataReducer {
  /** @inheritDoc */
  $module = Module.EVENTS

  /** @inheritDoc */
  getInitialState() {
    return {
      ...super.getInitialState(),
      data: ''
    }
  }
}

/**
 * A reducer for upcoming events
 * @type {EventsReducer}
 */
const eventsReducer = new EventsReducer()

export default eventsReducer
