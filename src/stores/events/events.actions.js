import Module from '../module.enum'
import DataActions from '../actions/data-actions'

/** All actions pertaining to upcoming events */
class EventsActions extends DataActions {
  /** @inheritDoc */
  $module = Module.EVENTS
  /** @inheritDoc */
  $collectionId = 'site-data'
  /** @inheritDoc */
  $docId = 'events'

  /** @inheritDoc */
  $convertDocToData(documentSnapshot) {
    return documentSnapshot.data().content
  }
}

/**
 * Actions pertaining to upcoming events
 * @type {EventsActions}
 */
const eventsActions = new EventsActions()

export const fetchEvents = eventsActions.fetch
