import StringLengthError from '../errors/stringLength.error'
import NullValueError from '../errors/nullValue.error'
import StringFormatError, { StringFormat } from '../errors/stringFormat.error'

/**
 * Capitalizes a string
 * @param {string} source - string to capitalize
 * @returns {string} capitalized version of source
 */
export function capitalize(source) {
  if (!source) return source
  return source[0].toUpperCase() + source.substring(1)
}

/**
 * Uncapitalizes a string
 * @param {string} source - string to uncapitalize
 * @return {string} uncapitalized version of source
 */
export function uncapitalize(source) {
  if (!source) return source
  return source[0].toLowerCase() + source.substring(1)
}

/**
 * Converts string to a readable one, splitting separated words, capitalizing the first and uncapitalizing the rest
 * @param {string} source - string to convert to a readable string
 * @returns {string} readable version of source
 */
export function toReadable(source) {
  if (!source) return ''
  const words = (source.match(/[A-Za-z][a-z]*/g) || [])
  words[0] = capitalize(words[0])
  for (let i = 1; i < words.length; ++i)
    words[i] = uncapitalize(words[i])
  return words.join(' ')
}

/**
 * Determines if a string is a valid email address
 * @param {string} source - the string to validate
 * @returns {boolean} whether or not source is a valid email
 */
export function isValidEmail(source) {
  if (!source) return false
  return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(source)
}

/**
 * Determines if a string is a valid US phone number
 * @param {string} source - the string to validate
 * @returns {boolean} whether or not source is a valid US phone number
 */
export function isValidUsPhone(source) {
  if (!source) return false
  return /^(\+?1[ -]?)?(\(?[0-9]{3}\)?)?[ -]?[0-9]{3}[ -]?[0-9]{4}$/.test(source)
}

/**
 * Extracts numbers from string returns a string of numbers (or empty string)
 * @param {string} source - the string to extract numbers from
 * @returns {string} a string of only the numbers in source or empty string
 */
export function extractNumbers(source) {
  if (!source) return ''
  return (source.match(/\d+/g) || []).join('')
}

/**
 * Determines if string is a string of numbers
 * @param {string} source - string to test
 * @returns {boolean} whether or not source consists only of numbers
 */
export function isNumbers(source) {
  if (!source) return false
  const chars = source.split('')
  for (let i = 0; i < chars.length; ++i)
    if (isNaN(+chars[i])) return false
  return true
}

/**
 * Converts a string of numbers to the specified format
 * @example
 * toPhoneFormat('18008675309', '+1(3)3-4') => '+1(800)867-5309'
 * @param {string} source - string of numbers
 * @param {string} format - format string
 * @returns {string} source numbers in specified format
 * @throws {NullValueError} when either parameter is null, undefined, or empty string
 * @throws {StringFormatError} when {@link isNumbers}(source) is false
 * @throws {StringLengthError} when source has less characters than format requires
 */
export function toPhoneFormat(source, format) {
  // Validation
  if (!source) throw new NullValueError('source')
  if (!isNumbers(source)) throw new StringFormatError('source', StringFormat.NUMBERS)
  if (!format) throw new NullValueError('format')
  const expectedLength = extractNumbers(format).split('').reduce((acc, cur) => acc + +cur, 0)
  if (source.length !== expectedLength) throw new StringLengthError('source', expectedLength, source.length)

  // Format
  let i = 0
  return format.split('').reduce((acc, cur) => acc + (+cur ? source.substring(i, i += +cur) : cur), '')
}
