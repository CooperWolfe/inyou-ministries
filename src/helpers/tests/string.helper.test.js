import * as Helper from '../string.helper'
import NullValueError from '../../errors/nullValue.error'
import { StringFormat } from '../../errors/stringFormat.error'
import StringFormatError from '../../errors/stringFormat.error'
import StringLengthError from '../../errors/stringLength.error'

// Capitalize
test('capitalizes string', () => {
  let uncapitalizedString = 'hello'
  let capitalizedString = 'Hello'

  uncapitalizedString = Helper.capitalize(uncapitalizedString)
  capitalizedString = Helper.capitalize(capitalizedString)

  expect(uncapitalizedString).toBe('Hello')
  expect(capitalizedString).toBe('Hello')
})
test('doesn\'t capitalize non-alphas', () => {
  let numeric = '123'
  let nonAlphaNumeric = '-hey'
  let nullString = null

  numeric = Helper.capitalize(numeric)
  nonAlphaNumeric = Helper.capitalize(nonAlphaNumeric)
  nullString = Helper.capitalize(nullString)

  expect(numeric).toBe('123')
  expect(nonAlphaNumeric).toBe('-hey')
  expect(nullString).toBe(null)
})

// Uncapitalize
test('uncapitalizes string', () => {
  let capitalizedString = 'Hello'
  let uncapitalizedString = 'hello'

  capitalizedString = Helper.uncapitalize(capitalizedString)
  uncapitalizedString = Helper.uncapitalize(uncapitalizedString)

  expect(capitalizedString).toBe('hello')
  expect(uncapitalizedString).toBe('hello')
})
test('doesn\'t uncapitalize non-alphas', () => {
  let numeric = '123'
  let nonAlphaNumeric = '-hey'
  let nullString = null

  numeric = Helper.uncapitalize(numeric)
  nonAlphaNumeric = Helper.uncapitalize(nonAlphaNumeric)
  nullString = Helper.uncapitalize(nullString)

  expect(numeric).toBe('123')
  expect(nonAlphaNumeric).toBe('-hey')
  expect(nullString).toBe(null)
})

// To Readable
test('makes camel case readable', () => {
  let uncapitalizedCamelCase = 'camelCase'
  let capitalizedCamelCase = 'CamelCase'

  uncapitalizedCamelCase = Helper.toReadable(uncapitalizedCamelCase)
  capitalizedCamelCase = Helper.toReadable(capitalizedCamelCase)

  expect(uncapitalizedCamelCase).toBe('Camel case')
  expect(capitalizedCamelCase).toBe('Camel case')
})
test('makes snake case readable', () => {
  let uncapitalizedSnakeCase = 'snake_case'
  let capitalizedSnakeCase = 'Snake_Case'

  uncapitalizedSnakeCase = Helper.toReadable(uncapitalizedSnakeCase)
  capitalizedSnakeCase = Helper.toReadable(capitalizedSnakeCase)

  expect(uncapitalizedSnakeCase).toBe('Snake case')
  expect(capitalizedSnakeCase).toBe('Snake case')
})
test('makes regular test readable', () => {
  let uncapitalizedRegularCase = 'regular case'
  let capitalizedRegularCase = 'Regular Case'

  uncapitalizedRegularCase = Helper.toReadable(uncapitalizedRegularCase)
  capitalizedRegularCase = Helper.toReadable(capitalizedRegularCase)

  expect(uncapitalizedRegularCase).toBe('Regular case')
  expect(capitalizedRegularCase).toBe('Regular case')
})
test('skips non-alpha', () => {
  let numeric = '123'
  let alphanumeric = 'Hi123There90'
  let nonalphanumeric = '-+=[]'
  let mixed = 'hey+there+pretty+lady+#1'

  numeric = Helper.toReadable(numeric)
  alphanumeric = Helper.toReadable(alphanumeric)
  nonalphanumeric = Helper.toReadable(nonalphanumeric)
  mixed = Helper.toReadable(mixed)

  expect(numeric).toBe('')
  expect(alphanumeric).toBe('Hi there')
  expect(nonalphanumeric).toBe('')
  expect(mixed).toBe('Hey there pretty lady')
})

// Is Valid Email
test('validates emails', () => {
  let standard = 'abc@email.com'
  let oddTld = 'abc@email.odc'
  let withNumbers = 'abc123@email123.com'
  let withNonalphanumerics = 'abc-xyz@email.com'
  let subdomain = 'abc@email.test.com'
  let withDots = 'abc.xyz@email.com'

  standard = Helper.isValidEmail(standard)
  oddTld = Helper.isValidEmail(oddTld)
  withNumbers = Helper.isValidEmail(withNumbers)
  withNonalphanumerics = Helper.isValidEmail(withNonalphanumerics)
  subdomain = Helper.isValidEmail(subdomain)
  withDots = Helper.isValidEmail(withDots)

  expect(standard).toBeTruthy()
  expect(oddTld).toBeTruthy()
  expect(withNumbers).toBeTruthy()
  expect(withNonalphanumerics).toBeTruthy()
  expect(subdomain).toBeTruthy()
  expect(withDots).toBeTruthy()
})
test('catches invalid emails', () => {
  let notEmail = 'hey'
  let noTld = 'abc@email'
  let empty = ''
  let nullString = null
  let withNumberInTld = 'abc@email.co1'

  notEmail = Helper.isValidEmail(notEmail)
  noTld = Helper.isValidEmail(noTld)
  empty = Helper.isValidEmail(empty)
  nullString = Helper.isValidEmail(nullString)
  withNumberInTld = Helper.isValidEmail(withNumberInTld)

  expect(notEmail).toBeFalsy()
  expect(noTld).toBeFalsy()
  expect(empty).toBeFalsy()
  expect(nullString).toBeFalsy()
  expect(withNumberInTld).toBeFalsy()
})

// Is Valid Phone
test('validates phones', () => {
  let withLeadingPlus = '+18648675309'
  let withoutLeadingPlus = '18648675309'
  let withoutCountryCode = '8648675309'
  let withoutAreaCode = '8675309'
  let withDashes = '1-864-867-5309'
  let withSpaces = '1 864 867 5309'
  let mixed = '+1 (864) 867-5309'

  withLeadingPlus = Helper.isValidUsPhone(withLeadingPlus)
  withoutLeadingPlus = Helper.isValidUsPhone(withoutLeadingPlus)
  withoutCountryCode = Helper.isValidUsPhone(withoutCountryCode)
  withoutAreaCode = Helper.isValidUsPhone(withoutAreaCode)
  withDashes = Helper.isValidUsPhone(withDashes)
  withSpaces = Helper.isValidUsPhone(withSpaces)
  mixed = Helper.isValidUsPhone(mixed)

  expect(withLeadingPlus).toBeTruthy()
  expect(withoutLeadingPlus).toBeTruthy()
  expect(withoutCountryCode).toBeTruthy()
  expect(withoutAreaCode).toBeTruthy()
  expect(withDashes).toBeTruthy()
  expect(withSpaces).toBeTruthy()
  expect(mixed).toBeTruthy()
})
test('catches invalid phones', () => {
  let invalidPhone = 'hello'
  let wrongNumberOfCharacters = '648675309'
  let multiplePluses = '++18648675309'
  let tooManySpaces = '1864867  5309'
  let invalidCharacter = '864|8675309'
  let empty = ''
  let nullString = null

  invalidPhone = Helper.isValidUsPhone(invalidPhone)
  wrongNumberOfCharacters = Helper.isValidUsPhone(wrongNumberOfCharacters)
  multiplePluses = Helper.isValidUsPhone(multiplePluses)
  tooManySpaces = Helper.isValidUsPhone(tooManySpaces)
  invalidCharacter = Helper.isValidUsPhone(invalidCharacter)
  empty = Helper.isValidUsPhone(empty)
  nullString = Helper.isValidUsPhone(nullString)

  expect(invalidPhone).toBeFalsy()
  expect(wrongNumberOfCharacters).toBeFalsy()
  expect(multiplePluses).toBeFalsy()
  expect(tooManySpaces).toBeFalsy()
  expect(invalidCharacter).toBeFalsy()
  expect(empty).toBeFalsy()
  expect(nullString).toBeFalsy()
})

// Extract Numbers
test('extracts numbers', () => {
  let numbers = '1234567890'
  let alphanumeric = 'a1b2c3'
  let mixed = 'a:1, b:2, c:3'

  numbers = Helper.extractNumbers(numbers)
  alphanumeric = Helper.extractNumbers(alphanumeric)
  mixed = Helper.extractNumbers(mixed)

  expect(numbers).toBe('1234567890')
  expect(alphanumeric).toBe('123')
  expect(mixed).toBe('123')
})
test('empty string when input has no numbers', () => {
  let empty = ''
  let nullString = null
  let alpha = 'abc'

  empty = Helper.extractNumbers(empty)
  nullString = Helper.extractNumbers(nullString)
  alpha = Helper.extractNumbers(alpha)

  expect(empty).toBe('')
  expect(nullString).toBe('')
  expect(alpha).toBe('')
})

// Is Numbers
test('detects numbers', () => {
  let numbers = '123'

  numbers = Helper.isNumbers(numbers)

  expect(numbers).toBeTruthy()
})
test('detects absence of numbers', () => {
  let noNumbers = 'a-b-c'
  let empty = ''
  let nullString = null

  noNumbers = Helper.isNumbers(noNumbers)
  empty = Helper.isNumbers(empty)
  nullString = Helper.isNumbers(nullString)

  expect(noNumbers).toBeFalsy()
  expect(empty).toBeFalsy()
  expect(nullString).toBeFalsy()
})

// To Phone Format
test('formats correctly', () => {
  const number11 = '18648675309'
  const format11 = '+1(3)3-4'
  const number10 = '8648675309'
  const format10 = '3 3 4'
  const number7 = '8675309'
  const format7 = '7'

  const formatted11 = Helper.toPhoneFormat(number11, format11)
  const formatted10 = Helper.toPhoneFormat(number10, format10)
  const formatted7 = Helper.toPhoneFormat(number7, format7)

  expect(formatted11).toBe('+1(864)867-5309')
  expect(formatted10).toBe('864 867 5309')
  expect(formatted7).toBe(number7)
})
test('throws on invalid input', () => {
  const nullSourceError = new NullValueError('source')
  const nullFormatError = new NullValueError('format')
  const stringFormatError = new StringFormatError('source', StringFormat.NUMBERS)
  const stringLengthError = new StringLengthError('source', 10, 11)
  const nullString = null
  const empty = ''
  const alpha = 'abc'
  const source11 = '18648675309'
  const format10 = '(3)3-4'

  expect(() => Helper.toPhoneFormat(nullString, null)).toThrowError(nullSourceError)
  expect(() => Helper.toPhoneFormat(empty, null)).toThrowError(nullSourceError)
  expect(() => Helper.toPhoneFormat(source11, null)).toThrowError(nullFormatError)
  expect(() => Helper.toPhoneFormat(source11, empty)).toThrowError(nullFormatError)
  expect(() => Helper.toPhoneFormat(alpha, format10)).toThrowError(stringFormatError)
  expect(() => Helper.toPhoneFormat(source11, format10)).toThrowError(stringLengthError)
})
