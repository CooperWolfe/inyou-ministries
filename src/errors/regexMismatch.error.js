/** An error thrown when a field fails to match a regex pattern */
export default class RegexMismatchError extends Error {
  /**
   * @returns RegexMismatchError
   * @param {string} field - the field that failed to match the regex pattern
   * @param {string=} customMessage - an optional custom message in case the default does not fit your needs
   */
  constructor(field, customMessage) {
    super()
    this.message = customMessage || `${field} was not in the correct format`
  }
}
