/** An error thrown when a function has not been implemented */
export default class NotImplementedError extends Error {
  /**
   * @returns NotImplementedError
   * @param {string} functionName - the name of the function that has not been implemented
   * @param {string=} customMessage - an optional custom error message in case the existing one is not sufficient
   */
  constructor(functionName, customMessage) {
    super()
    this.message = customMessage || `The ${functionName} function has not been implemented`
  }
}
