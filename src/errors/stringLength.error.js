/** Error that occurs when a string is of the wrong length */
export default class StringLengthError extends Error {
  /**
   * @returns StringLengthError
   * @param stringName - string - the name of the string with the wrong length
   * @param expectedLength - number - expected length of the string
   * @param actualLength - number - actual length of the string
   * @param customMessage - string? - an optional custom message in case the default doesn't fit your needs
   */
  constructor(stringName, expectedLength, actualLength, customMessage) {
    super()
    this.message = customMessage || `${stringName} should be a different length | Expected: ${expectedLength} | Actual: ${actualLength}`
  }
}
