/** Error thrown when a required value is not present */
export default class NullValueError extends Error {
  /**
   * @returns NullValueError
   * @param {string} field - the field that was unexpectedly not present
   * @param {string=} customMessage - an optional custom error message in case the existing one is not sufficient
   */
  constructor(field, customMessage) {
    super()
    this.message = customMessage || `${field} is required`
  }
}
