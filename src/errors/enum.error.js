/** An error that occurs when something expected to exist in an enum does not */
export default class EnumError extends Error {
  /**
   * @returns {EnumError}
   * @param {string} field - the name of the field on which this error occurred
   * @param {string} enumName - the name of the enumeration of which the subject did not match
   * @param {Array<*>} options - the enumeration options, i.e. what the subject could have been
   * @param {string=} customMessage - an optional custom message in case the existing one is not sufficient
   */
  constructor(field, enumName, options, customMessage) {
    super()
    this.message = customMessage || `The field ${field} was not a valid ${enumName}. Expected ${options.join('|')}`
  }
}
