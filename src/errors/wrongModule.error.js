/** Error thrown when something is in the wrong module */
export default class WrongModuleError extends Error {
  /**
   * @returns WrongModuleError
   * @param {Module} expected - the expected module
   * @param {Module} actual - the actual module
   * @param {string=} customMessage - an optional custom message in case the default is insufficient
   */
  constructor(expected, actual, customMessage) {
    super()
    this.message = customMessage || `Wrong module - expected: ${expected}, actual: ${actual}`
  }
}
