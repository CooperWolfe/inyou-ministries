/**
 * @enum {string}
 * A description of what a string might look like
 */
export const StringFormat = {
  NUMBERS: 'all numbers'
}

/** Error thrown when a string is in the incorrect format */
export default class StringFormatError extends Error {
  /**
   * @returns StringFormatError
   * @param {string} field - the field that had the incorrect format
   * @param {StringFormat} format - a description of the desired format of the field
   * @param {string=} customMessage - an optional custom error message in case the default is insufficient
   */
  constructor(field, format, customMessage) {
    super()
    this.message = customMessage || `${field} was not in the correct format: ${format}`
  }
}
