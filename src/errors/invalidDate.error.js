/** An error thrown when an invalid date value is unexpectedly found */
export default class InvalidDateError extends Error {
  /**
   * @returns InvalidDateError
   * @param {string} paramName - The name of the parameter that was unexpectedly an invalid date
   * @param {*} value - The invalid date value
   * @param {string=} customMessage - An optional custom error message in case the default is insufficient
   */
  constructor(paramName, value, customMessage) {
    super()
    this.message = customMessage || `${paramName} with value ${value} is not a valid date`
  }
}
