import Model from './model'
import NullValueError from '../errors/nullValue.error'
import EnumError from '../errors/enum.error'

/**
 * The type of C
 * @example conference, counsel, or curricula
 * @enum {string}
 */
export const CType = {
  conference: 'conference',
  counsel: 'counsel',
  curricula: 'curricula'
}

/**
 * One of the C's of InYou
 * @example Conference, Counsel, Curricula
 */
export default class C extends Model {
  /** @private string */ _description
  /** @type string */
  get description() { return this._description }
  set description(value) {
    if (!value) throw new NullValueError('Description')
    this._description = value
  }

  /** @private string */ _imageUrl
  /** @type string */
  get imageUrl() { return this._imageUrl }
  set imageUrl(value) {
    if (!value) throw new NullValueError('Image URL')
    this._imageUrl = value
  }

  /**
   * @returns C
   * @param {string} description - the C's description
   * @param {string} imageUrl - the image URL for the C
   * @param {string} id - the ID of the C
   */
  constructor(description, imageUrl, id) {
    if (!Object.keys(CType).includes(id)) throw new EnumError('id', 'CType', Object.keys(CType))
    super(id)
    this.description = description
    this.imageUrl = imageUrl
  }


  /**
   * @inheritDoc
   * @returns {C} A new C object with the updated information
   */
  update(updateObject) {
    return new C(
      updateObject.description || this.description,
      updateObject.imageUrl || this.imageUrl,
      updateObject.id || this.id
    )
  }

  /**
   * Creates a C object from a document snapshot
   * @param {object} documentSnapshot - the document snapshot to use for creation
   * @returns {C} a C object created from data in a document snapshot
   */
  static fromDocumentSnapshot(documentSnapshot) {
    const data = {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
    return C.fromObject(data)
  }

  /**
   * Creates a C object from a regular object
   * @param {object} object - the regular object to use for creation
   * @returns {C} a model object with data from the createObject
   */
  static fromObject(object) {
    return new C(
      object.description,
      object.imageUrl,
      object.id
    )
  }
}
