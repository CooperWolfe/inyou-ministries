import News from './news.model'

/** An object representing a mailchimp campaign */
export default class Campaign extends News {
  /**
   * Creates a campaign object from a firebase document snapshot
   * @param {object} documentSnapshot - the document snapshot to convert from
   * @returns Campaign
   */
  static fromDocumentSnapshot(documentSnapshot) {
    const data = {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
    return Campaign.fromObject(data)
  }

  /**
   * Creates a new campaign object from a regular object
   * @param {object} createObject - the object to create the campaign object from
   * @returns Campaign
   */
  static fromObject(createObject) {
    return new Campaign(
      createObject.id,
      createObject.date,
      createObject.subject,
      createObject.url,
      createObject.imageUrl
    )
  }
}
