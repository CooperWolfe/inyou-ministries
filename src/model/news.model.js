import Model from './model'
import InvalidDateError from '../errors/invalidDate.error'
import NullValueError from '../errors/nullValue.error'

/** A model object describing the news section on the home page */
export default class News extends Model {
  /** @private Date */ _date
  /** @type Date */
  get date() { return this._date }
  set date(value) {
    const dateValue = new Date(value)
    if (dateValue.toString() === 'Invalid Date') throw new InvalidDateError('News.date', value)
    this._date = dateValue
  }

  /** @private string */ _subject
  /** @type string */
  get subject() { return this._subject }
  set subject(value) {
    if (!value) throw new NullValueError('News.subject')
    this._subject = value
  }

  /** @private string */ _url
  /** @type string */
  get url() { return this._url }
  set url(value) {
    this._url = value
  }

  /** @private string */ _imageUrl
  /** @type string */
  get imageUrl() { return this._imageUrl }
  set imageUrl(value) {
    this._imageUrl = value
  }

  /**
   * @returns News
   * @param {string} id - the model ID
   * @param {string|number|Date} date - the date the news was made public
   * @param {string} subject - the subject of the news
   * @param {string} url - the URL the news should take you to
   * @param {string} imageUrl - the URL of the image to accompany the news
   */
  constructor(id, date, subject, url, imageUrl) {
    super(id)
    this.date = date
    this.subject = subject
    this.url = url
    this.imageUrl = imageUrl
  }

  /**
   * Creates a new News object with updated information
   * @param {object} updateObject - the update object
   * @returns News
   */
  update(updateObject) {
    return new News(
      updateObject.id || this.id,
      updateObject.date || this.date,
      updateObject.subject || this.subject,
      updateObject.url || this.url,
      updateObject.imageUrl || this.imageUrl
    )
  }

  /**
   * Creates a news object from a firebase document snapshot
   * @param {object} documentSnapshot - the document snapshot to convert from
   * @returns News
   */
  static fromDocumentSnapshot(documentSnapshot) {
    const data = {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
    return News.fromObject(data)
  }

  /**
   * Creates a new News object from a regular object
   * @param {object} createObject - the object to create the News object from
   * @returns News
   */
  static fromObject(createObject) {
    return new News(
      createObject.id,
      createObject.date,
      createObject.subject,
      createObject.url,
      createObject.imageUrl
    )
  }
}
