import Model from './model'
import { isValidEmail, isValidUsPhone } from '../helpers/string.helper'
import RegexMismatchError from '../errors/regexMismatch.error'

/** An entity that stores contact information */
export default class ContactInfo extends Model {
  /** @private string */ _email
  /** @type string */
  get email() { return this._email }
  set email(value) {
    if (!isValidEmail(value)) throw new RegexMismatchError('Email')
    this._email = value
  }

  /** @private string */ _phone
  /** @type string */
  get phone() { return this._phone }
  set phone(value) {
    if (!isValidUsPhone(value)) throw new RegexMismatchError('Phone')
    this._phone = value
  }

  /** @private string */ _facebookUrl
  /** @type string */
  get facebookUrl() { return this._facebookUrl }
  set facebookUrl(value) {
    this._facebookUrl = value
  }

  /** @private string */ _twitterUrl
  /** @type string */
  get twitterUrl() { return this._twitterUrl }
  set twitterUrl(value) {
    this._twitterUrl = value
  }

  /** @private string */ _address1
  /** @type string */
  get address1() { return this._address1 }
  set address1(value) {
    this._address1 = value
  }

  /** @private string */ _address2
  /** @type string */
  get address2() { return this._address2 }
  set address2(value) {
    this._address2 = value
  }

  /** @private string */ _addressUrl
  /** @type string */
  get addressUrl() { return this._addressUrl }
  set addressUrl(value) {
    this._addressUrl = value
  }

  /** @private string */ _mailingAddress
  /** @type string */
  get mailingAddress() { return this._mailingAddress }
  set mailingAddress(value) {
    this._mailingAddress = value
  }

  /**
   * @returns ContactInfo
   * @param {string} email - Email address
   * @param {string} phone - Phone number
   * @param {string} facebookUrl - A Facebook URL
   * @param {string} twitterUrl - A Twitter URL
   * @param {string} youtubeUrl - A YouTube URL
   * @param {string} address1 - Location Address (first line)
   * @param {string} address2 - Location Address (second line)
   * @param {string} addressUrl - The Google Maps URL of the location address
   * @param {string} mailingAddress - Mailing Address
   */
  constructor(email, phone, facebookUrl, twitterUrl, address1, address2, addressUrl, mailingAddress) {
    super('contact-info')
    this.email = email
    this.phone = phone
    this.facebookUrl = facebookUrl
    this.twitterUrl = twitterUrl
    this.address1 = address1
    this.address2 = address2
    this.addressUrl = addressUrl
    this.mailingAddress = mailingAddress
  }


  /**
   * @inheritDoc
   * @returns {ContactInfo} A new contact info object with data from the update object
   */
  update(updateObject) {
    return new ContactInfo(
      updateObject.email || this.email,
      updateObject.phone || this.phone,
      updateObject.facebookUrl || this.facebookUrl,
      updateObject.twitterUrl || this.twitterUrl,
      updateObject.address1 || this.address1,
      updateObject.address2 || this.address2,
      updateObject.addressUrl || this.addressUrl,
      updateObject.mailingAddress || this.mailingAddress
    )
  }

  /**
   * Creates a contact info object from a document snapshot
   * @param {object} documentSnapshot - the document snapshot from which to create the contact info
   * @returns {ContactInfo} A model object created from data in a document snapshot
   */
  static fromDocumentSnapshot(documentSnapshot) {
    const data = documentSnapshot.data()
    return ContactInfo.fromObject(data)
  }

  /**
   * Creates a contact info object from a regular object
   * @param {object} object - the object from which to create the contact info
   * @returns {ContactInfo} A contact info object with data from the createObject
   */
  static fromObject(object) {
    const addressUrl = /Apple|iPhone|iPod|iPad|iOS/.test(navigator.userAgent)
      ? object['appleAddressUrl'] || object.addressUrl
      : object.addressUrl

    return new ContactInfo(
      object.email,
      object.phone,
      object.facebookUrl,
      object.twitterUrl,
      object.address1,
      object.address2,
      addressUrl,
      object.mailingAddress
    )
  }
}
