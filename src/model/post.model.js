import News from './news.model'

/** An object representing an Instagram Post */
export default class Post extends News {
  /**
   * Creates a post object from a firebase document snapshot
   * @param {object} documentSnapshot - the document snapshot to convert from
   * @returns Post
   */
  static fromDocumentSnapshot(documentSnapshot) {
    const data = {
      id: documentSnapshot.id,
      ...documentSnapshot.data()
    }
    return Post.fromObject(data)
  }

  /**
   * Creates a new post object from a regular object
   * @param {object} createObject - the object to create the post object from
   * @returns Post
   */
  static fromObject(createObject) {
    return new Post(
      createObject.id,
      createObject.date,
      createObject.subject,
      createObject.url,
      createObject.imageUrl
    )
  }
}
