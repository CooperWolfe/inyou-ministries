import uuid from 'uuid/v4'

/** An entity used for data persistance */
export default class Model {
  /** @private string */ _id
  /** @type string */
  get id() { return this._id }

  /**
   * @returns Model
   * @protected
   * @param {string=} id - The ID of the model (do not use during creation)
   */
  constructor(id) {
    this._id = id || uuid()
  }


  /**
   * Creates a new model object with updated data
   * @abstract
   * @param {object} updateObject - an object with the fields you want to update
   * @returns {Model} A new model object with the updated information
   */
  update(updateObject) {
    return new Model(
      updateObject.id || this.id
    )
  }

  /**
   * Converts the model to a persistence model (removes private underscores)
   * @returns {object} The model without private underscores
   */
  toPersistenceObject() {
    return Object.keys(this).reduce((acc, cur) => ({ ...acc, [cur.substr(1)]: this[cur] }), {})
  }

  /**
   * Creates a model object from a document snapshot
   * @abstract
   * @param {object} documentSnapshot - the document snapshot to use for object creation
   * @returns {Model} A model object created from data in a document snapshot
   */
  static fromDocumentSnapshot(documentSnapshot) {
    return new Model(documentSnapshot.id)
  }

  /**
   * Creates a model object from a regular object
   * @abstract
   * @param {object} createObject - The regular object to use for creation
   * @returns {Model} A model object with data from the createObject
   */
  static fromObject(createObject) {
    return new Model(createObject.id)
  }
}
